# Ejercicios Curso R EAN

Repositorio con los ejercicios del curso de R Ean Core Competences

Para revisar los apuntes utilizar el programa [cherrytree](https://www.giuspen.com/cherrytree/)

## Contenidos

- Sesión 1: Introducción
- Sesión 2:
- Sesión 3: 
    * Estructuras de Control
    * Estructuras de repeticion
    * Categorización
    * Modelos de Regresión
